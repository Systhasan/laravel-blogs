@extends("admin.master")

@section('content')
 <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Category
                        </div>
                        <div class="panel-body">
                            <div class="row">
                               
                                <div class="col-lg-6">
                                     <h3 class="text-success"> {{ Session::get('msg') }} </h3>
                                    {!! Form::open(['url'=>'category/save', 'method'=>'post', 'id'=>$category->id]) !!}
                                
                                        
                                       <div class="form-group">
                                            <label>Category Name</label>
                                            <input class="form-control" name="cat_name" value="{{$category->cat_name}}">
                                            <span>{{ $errors->has('cat_name')? $errors->first('cat_name') : '' }}</span>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Category Description</label>
                                            <input class="form-control" name="cat_desc" value="{{$category->cat_desc}}">
                                            <span>{{$errors->has('cat_desc')? $errors->first('cat_desc') : ''}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option value='1' {{$category->status==1?"selected":""}} >Publish</option>
                                                <option value='0' {{$category->status==0?"selected":""}} >Un-Publish</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-default">Save Category</button>
                                </div>
                                      
                                        
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
								
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection