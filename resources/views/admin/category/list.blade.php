@extends("admin.master")


@section('content')

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Categories</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Category Name</th>
                                        <th>Category Description</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($i=1)
                                    @foreach($categories as $category)
                                        <tr class="odd gradeX">
                                            <td>{{$i++}}</td>
                                            <td>{{$category->cat_name}}</td>
                                            <td>{{$category->cat_desc}}</td>
                                            <td>{{$category->status?'Publish':'Un-publish'}}</td>
                                            <td class="center"> 
                                                <a href="{{url('/category/edit/'.$category->id)}}" class="btn btn-success">Update</a>
                                                <a href="{{url('/category/delete/'.$category->id)}}" class="btn btn-danger">Delete</a>
                                                <a href="{{url('/category/status/'.$category->id)}}" class="btn btn-warning">Change Status To </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                          
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
      


@endsection
