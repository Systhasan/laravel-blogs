@include('admin.includes.head')

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      @include('admin.includes.header')
      @include('admin.includes.sidebar')  
    </nav>
    <div id="page-wrapper">
       @yield('content')
    </div>
    <!-- /#page-wrapper -->
</div>
    <!-- /#wrapper -->

<!-- jQuery -->
@include('admin.includes.footer')