@extends("admin.master")


@section('content')

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Products</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($i=1)
                                    @foreach($products as $product)
                                        <tr class="odd gradeX">
                                            <td>{{$i++}}</td>
                                            <td>{{$product->product_name}}</td>
                                            <td>{{$product->cat_name}}</td>
                                            <td>{{$product->product_description}}</td>
                                            <td>
                                                <img src="{{asset($product->product_image)}}" style="width:152px;height: 140px;">
                                            </td>
                                            <td>{{$product->status?'Publish':'Un-publish'}}</td>
                                            <td class="center"> 
                                                <a href="{{url('/product/edit/'.$product->id)}}" class="btn btn-success">Update</a>
                                                <a href="{{url('/product/delete/'.$product->id)}}" class="btn btn-danger">Delete</a>
                                                <a href="{{url('/product/status/'.$product->id)}}" class="btn btn-warning">Change Status To </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                          
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
      

@endsection
