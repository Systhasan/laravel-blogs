@extends("admin.master")

@section('content')
 <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Product
                        </div>
                        <div class="panel-body">
                            <div class="row">
                               
                                <div class="col-lg-6">
                                     <h3 class="text-success"> {{ Session::get('msg') }} </h3>
                                    {!! Form::open(['url'=>'product/save', 'method'=>'post', 'enctype' => 'multipart/form-data']) !!}
                                
                                        
                                        <div class="form-group">
                                            <label>Product Name</label>
                                            <input class="form-control" name="product_name" value="">
                                            <span>{{ $errors->has('product_name')? $errors->first('product_name') : '' }}</span>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Product Category</label>
                                                <select class="form-control" name="category">
                                                    @foreach($category as $cat)
                                                        <option value='{{ $cat->id }}'  selected>{{$cat->cat_name}}</option>
                                                    @endforeach
                                                </select>
                                            <span>{{ $errors->has('category')? $errors->first('category') : '' }}</span>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Product Description</label>
                                            <input class="form-control" name="product_desc" value="">
                                            <span>{{$errors->has('product_desc')? $errors->first('product_desc') : ''}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option value='1'  selected>Publish</option>
                                                <option value='0' >Un-Publish</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Product Image</label>
                                            <input type="file" name="product_image"/>
                                        </div>
                                        <button type="submit" class="btn btn-default">Save Product</button>
                                </div>
                                      
                                        
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.col-lg-6 (nested) -->
								
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection