@include('front-end.includes.header')

<!-- Page Preloder -->
<div id="preloder">
        <div class="loader"></div>
</div>

@yield('content')

@include('front-end.includes.footer')