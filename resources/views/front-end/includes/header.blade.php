<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Front-End</title>
	<meta charset="UTF-8">
	<meta name="description" content=" Divisima | eCommerce Template">
	<meta name="keywords" content="divisima, eCommerce, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="{{ asset('/') }}front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/flaticon.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/slicknav.min.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/jquery-ui.min.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/animate.css"/>
	<link rel="stylesheet" href="{{ asset('/') }}front-end/css/style.css"/>


	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>