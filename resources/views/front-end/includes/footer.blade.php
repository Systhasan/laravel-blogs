	<!--====== Javascripts & Jquery ======-->
	<script src="{{ asset('/') }}front-end/js/jquery-3.2.1.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/bootstrap.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/jquery.slicknav.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/owl.carousel.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/jquery.nicescroll.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/jquery.zoom.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/jquery-ui.min.js"></script>
	<script src="{{ asset('/') }}front-end/js/main.js"></script>

	</body>
</html>
