<?php

Route::get('/',[
  'uses' => 'BlogController@index',
  'as'   => '/'
]);

Route::get('/dashboard',[
  'uses' => 'dashboardController@index',
  'as'   => '/'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Category
Route::get('/category/add', 'CategoryController@addCategory');
Route::get('/category/manage', 'CategoryController@manageCategory');
Route::post('/category/save', 'CategoryController@saveCategory');
Route::get('/category/edit/{id}', 'CategoryController@edit');
Route::get('/category/delete/{id}', 'CategoryController@delete');

// Brand 
Route::get('/brand/add', 'BrandController@index');

// product 
Route::get('/product/add', 'ProductController@addProduct');
Route::post('/product/save', 'ProductController@saveProduct');
Route::get('/product/list', 'ProductController@listProduct');
Route::get('/product/edit', 'ProductController@editProduct');

