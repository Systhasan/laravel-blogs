<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category; 
use Image; 
use DB; 
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function addProduct(){
        $category = Category::where('status',1)->get(); 
        
        return view('admin.product.add', compact('category'));
    }
    
    public function saveProduct(Request $request){
        // Form Field Validate
        $this->validate($request, ['product_name'=>'required', 'category'=>'required']);
        
        $product_image = $request->file('product_image');
        $image_name    = $product_image->getClientOriginalName();
        $directory     = 'uploads/';
        $image_url     = $directory.$image_name;
        Image::make($product_image)->save($image_url);
        
        $product = new Product();
        $product->product_name          = $request->product_name;
        $product->category              = $request->category;
        $product->product_description   = $request->product_desc;
        $product->status                = $request->status;
        $product->product_image         = $image_url;
        $product->save();
        
        return redirect('/product/add')->with('msg', 'Product Information Save Successfully.');
    }
    
    public function listProduct(){
        $products   =   DB::table('products')
                        ->join('categories', 'products.category', '=', 'categories.id')
                        ->select('products.*', 'categories.cat_name')
                        ->get();
        // $products = Product::all();
        return view('admin.product.list',['products'=>$products]);
    }
}

