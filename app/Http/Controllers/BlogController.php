<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category; 

class BlogController extends Controller{

    public function index(){
      // get all active category.   
      $category = Category::where('status',1)->get();
      return view('front-end.home.home', compact('category'));
    }
    
}
