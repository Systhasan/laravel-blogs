<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DB;

class CategoryController extends Controller
{
  
   
   /*
    * Using this method store category data to the database.
    * 
    * @method  saveCategory
    * @param   -
    * @return  void
    */
   public function saveCategory(Request $request ){
        $this->validate($request, ['cat_name'=>'required', 'cat_desc'=>'required']);
        
        // 1st ORM process, to store data to table.
        $category = new Category();
        $category->cat_name = $request->cat_name;
        $category->cat_desc = $request->cat_desc;
        $category->status = $request->status;
        $category->save();
       
       // 2nd ORM process, to store data to table. 
       // Category::create($request->all());
        
      
       // 3rd Query Builder process.
       //DB::table('categories')->insert([
       //    'cat_name'   => $request->cat_name,
       //    'cat_desc'   => $request->cat_desc,
       //    'status'     => $request->status,
       //]);
       return redirect('/category/add')->with('msg', 'Category Information Save Successfully.');
   }
      
   
   public function addCategory(){
       // create a standard object
       return view('admin.category.add',['category'=>$this->getEmptyField()]);
   }
   
   public function manageCategory(){
       // get all category data. 
       $categories = Category::all();
       return view('admin.category.list', ['categories'=>$categories]);
   }
   
   public function edit($id){
      // get single category data using category id. 
      $category = Category::find($id);
      return view('admin.category.add', ['category'=>$category]);
   }
   
   public function delete($id){
       // delete data using id. 
       $category = Category::where('id', $id)->delete();
       return redirect('/category/manage')->with('msg', 'Category Delete Successfully.');
   }
   
   private function getEmptyField(){
       return (object) ['id'=>'', 'cat_name'=>'', 'cat_desc'=>'', 'status'=>1];
   }
   
   
   
   
   
   
   
}


